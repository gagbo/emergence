/* Copyright (c) 2018 Gerry Agbobada
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef _ENTITY_MOVEMENT_STRATEGY_MOVEMENT_STRATEGY_CAPFORCE_H_
#define _ENTITY_MOVEMENT_STRATEGY_MOVEMENT_STRATEGY_CAPFORCE_H_
#include "entity/ant/ant.h"
#include "movement_strategy_composite.h"

// Strategy which caps the force in case it got too strong in the behaviour
// items.
class MovementStrategyCapForce : public MovementStrategyComposite {
 public:
    MovementStrategyCapForce() : MovementStrategyComposite() {}
    MovementStrategyCapForce(std::unique_ptr<MovementStrategy> wrappee)
        : MovementStrategyComposite(std::move(wrappee)) {}
    void compute_force(Ant* context) override;
    ~MovementStrategyCapForce() {}
};
#endif  // _ENTITY_MOVEMENT_STRATEGY_MOVEMENT_STRATEGY_CAPFORCE_H_
